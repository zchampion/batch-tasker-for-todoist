# Batch Tasker for Todoist

Using the Todoist task management platform, it's difficult to create a plan with more than one task/subtask in it, making sure each task/subtask has the proper labels, is in the proper project, and has the proper priority assigned to it.

I have a skeleton of a project, written in python and using some basic tkinter, that takes a yaml-formatted text or file and uses the Todoist API to create tasks & subtasks, all with the correct priority, project, due date, and labels, making Todoist feasible to use as a current task tracker/plan creator.

This project aims to continue to develop that skeleton and make it from a POC into something resembling a final project.
