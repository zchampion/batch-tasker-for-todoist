"""
 _____         _       ____                _
|_   _|_ _ ___| | __  / ___|_ __ ___  __ _| |_ ___  _ __
  | |/ _` / __| |/ / | |   | '__/ _ \/ _` | __/ _ \| '__|
  | | (_| \__ \   <  | |___| | |  __/ (_| | || (_) | |
  |_|\__,_|___/_|\_\  \____|_|  \___|\__,_|\__\___/|_|
"""
import os
import sys
import threading
import time
from copy import deepcopy
from tkinter import simpledialog

import todoist
import yaml
from cryptography.fernet import Fernet

from TaskCreator.utils import get_logger

log = get_logger(__name__)


class TaskCreator:
    def __init__(self):
        __key = self._get_api_key()
        while __key is None:
            log.error(f"Todoist API Key Missing")
            __key = self._get_api_key()

        self.api = todoist.TodoistAPI(__key)
        while 'error' in self.api.sync():
            __key = self.write_new_token()
            self.api = todoist.TodoistAPI(__key)

        # dictionaries have ids as the keys and names as the values
        self.valid_projects = {proj['name']: proj['id'] for proj in self.api.state['projects']}
        log.debug(f'Valid projects: {self.valid_projects}')
        self.valid_labels = {lab['name']: lab['id'] for lab in self.api.state['labels']}
        log.debug(f'Valid labels: {self.valid_labels}')
        self.all_config = self._validate_config(None)

        self.TASK_CREATE_LOCK = False

    def _time_task_lock(self):
        time.sleep(2)
        self.TASK_CREATE_LOCK = False

    @staticmethod
    def _load_yaml(filename):
        log.debug(f'Loading yaml from {os.path.join(os.path.abspath(os.curdir), filename)}')

        try:
            with open(filename, 'r') as reader:
                loaded = yaml.load_all(reader, Loader=yaml.SafeLoader)

            return loaded

        except Exception as e:
            log.error(f"{type(e).__name__} error loading yaml from file: {e}")
            return None

    def load_tasks_from_yaml(self, filename):
        tasks_yaml = self._load_yaml(filename)
        log.debug(f'Loaded tasks from yaml: {tasks_yaml}')

        return tasks_yaml

    @staticmethod
    def load_tasks_from_yaml_text(text):
        try:
            return yaml.load_all(text, Loader=yaml.SafeLoader)
        except Exception as e:
            log.error(f"{type(e).__name__} error loading tasks from yaml: {e}")
            raise e

    def _get_api_key(self):
        try:
            # Get the value
            log.debug(f'Loading key...')
            with open('config.yaml', 'r') as reader:
                config = yaml.load(reader, Loader=yaml.SafeLoader)
            token, key = config['todoist']

            token = token.encode('utf8')

            cipher_suite = Fernet(key)
            decoded_token = cipher_suite.decrypt(token).decode('utf8')

            return decoded_token

        except (FileNotFoundError, TypeError) as e:
            log.error(f"Config file not found or key not present in config due to {type(e).__name__}: {e}")

            token = self.write_new_token()

            return token

        except Exception as e:
            msg = f"API Key not found due to {type(e).__name__} - {e}"
            log.error(msg)
            raise ValueError(msg)

    def write_new_token(self):
        # Set the value
        key = Fernet.generate_key()
        cipher_suite = Fernet(key)
        with open('config.yaml', 'w') as pen:
            token = simpledialog.askstring("Todoist API Key Not Found", "Key:")
            config = {
                'todoist': [
                    str(cipher_suite.encrypt(bytes(token, 'utf8')))[2:-1],
                    str(key)[2:-1]
                ]
            }
            pen.write(yaml.dump(config))
        return token

    def _validate_config(self, task_attributes):
        """
        Loads task attributes and returns the dict
        :param task_attributes:
        :return:
        """
        valid = {
            'labels': []
        }

        if task_attributes is None:
            return valid
        if 'due' in task_attributes:
            valid['due'] = task_attributes['due']
        if 'labels' in task_attributes:
            valid['labels'] = [self.valid_labels[label] for label in task_attributes['labels'] if label in self.valid_labels]
            for label in task_attributes['labels']:
                if label not in self.valid_labels:
                    log.warning(f'Label {label} is not a valid label!')
        if 'project' in task_attributes and task_attributes['project'] in self.valid_projects:
            valid['project_id'] = self.valid_projects[task_attributes['project']]
        if 'priority' in task_attributes and task_attributes['priority'] in [i for i in range(1, 5)]:
            valid['priority'] = task_attributes['priority']

        log.debug(f'Configuration validated: {valid}')

        return valid

    def _process_node(self, node, parent=None, status_signal=None):
        """

        :param node:
        :param parent:
        :return:
        """
        log.debug(f'Found {type(node).__name__} node with parent {parent}!')

        if type(node) == dict:
            keys_to_check = list(node.keys())
            subtasks = None

            if 'task' in keys_to_check:
                if 'subtasks' in keys_to_check:
                    subtasks = node.pop('subtasks')

                parent_task = self._process_task(node, parent=parent, status_signal=status_signal)

                if subtasks:
                    for task in subtasks:
                        self._process_node(task, parent=parent_task, status_signal=status_signal)

            else:
                for key in node:
                    parent_id = self._process_node(key, parent=parent, status_signal=status_signal)
                    self._process_node(node[key], parent=parent_id, status_signal=status_signal)

        elif type(node) == list:
            for item in node:
                self._process_node(item, parent=parent, status_signal=status_signal)

        elif type(node) == str:
            return self._process_task(node, parent=parent, status_signal=status_signal)

        elif type(node) == int:
            return self._process_task(str(node), parent=parent, status_signal=status_signal)

        else:
            log.warning(f'Found {type(node).__name__}!')

    def _process_task(self, task, parent=None, status_signal=None):
        """
        Process the node as if it's already been determined to be wholly a task.

        info:
        >> api.items.add('things and stuff', labels=['subtask'])
        Item({'content': 'things and stuff',
         'id': '7eb1a748-0543-11eb-a3c4-e1334f2956fc',
         'labels': ['subtask'],
         'project_id': 377531891})

        :param task:
        :return:
        """
        wait_counter = 0
        while self.TASK_CREATE_LOCK:
            if wait_counter > 100:
                raise TimeoutError(f'Timed out waiting to create {task}!')
            time.sleep(0.1)
            wait_counter += 1

        log.debug(f'Processing {type(task).__name__} task: {task} - with parent: {parent}')

        if parent:
            parent_task_id = parent['id']
        else:
            parent_task_id = None

        if type(task) == dict:
            content = task.pop('task')
            task_config = deepcopy(self.all_config)
            task_config.update(self._validate_config(task))
            log.debug(f'Content popped! Task config remaining: {task_config}')
            if parent_task_id is not None:
                if 'labels' not in task_config:
                    task_config['labels'] = []
                if 'due' in task_config:
                    task_config.pop('due')
                task_config['labels'].append(self.valid_labels['subtask'])
            log.debug(f'After ensuring label is present for subtasks: {task_config}')
            item = self.api.items.add(content, parent_id=parent_task_id, **task_config)
            threading.Thread(self._time_task_lock())
            log.info(f'Item added: {item["content"]}')
            log.debug(f'Item added: {item}')
            status_signal.emit(f"Sending {item['content']}...")
            return item

        elif type(task) == str:
            if self.all_config:
                task_config = deepcopy(self.all_config)
                if parent_task_id is not None:
                    if 'due' in task_config:
                        task_config.pop('due')
                    task_config['labels'].append(self.valid_labels['subtask'])
                item = self.api.items.add(task, parent_id=parent_task_id, **task_config)
            else:
                item = self.api.items.add(task, parent_id=parent_task_id, labels=self.valid_labels['subtask'])

            threading.Thread(self._time_task_lock())
            log.info(f'Item added: {item["content"]}')
            log.debug(f'Item content: {item}')
            status_signal.emit(f"Sending {item['content']}...")
            return item

        else:
            log.warning(f'What the hell? Task is a {type(task).__name__}!')

    def create_tasks_from_yaml(self, from_yaml, status_signal=None):
        if 'all' not in from_yaml and 'tasks' not in from_yaml:
            self._process_node(from_yaml, status_signal=status_signal)
            commit_result = self.api.commit()
            log.info(f"Successfully sent tasks to Todoist!")
        elif 'all' in from_yaml and 'tasks' in from_yaml:
            self.all_config = self._validate_config(from_yaml['all'])
            self._process_node(from_yaml['tasks'], status_signal=status_signal)
            commit_result = self.api.commit()
            success_msg = f"Successfully sent tasks to Todoist!"
            log.info(success_msg)
            status_signal.emit(success_msg)
        else:
            log.error(f'Invalid yaml configuration. Needs "all" and "tasks" or neither.')


if __name__ == "__main__":
    tc = TaskCreator()
    if len(sys.argv) > 1:
        file = sys.argv[1]
    else:
        file = '../tests/data/taskstocreate.yaml'
    file_yaml = tc.load_tasks_from_yaml(file)
    for i, document in enumerate(file_yaml):
        tc.create_tasks_from_yaml(document)
