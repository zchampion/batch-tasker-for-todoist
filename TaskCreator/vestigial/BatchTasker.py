from tkinter.filedialog import *

from TaskCreator.taskCreator import TaskCreator
from TaskCreator.utils import get_logger

log = get_logger(__name__)


class TaskEditor:
    def __init__(self, root=Tk()):
        # default window width and height
        self.default_width = 300
        self.default_height = 300
        self.text_area = Text(root)

        self.text_scrollbar = Scrollbar(self.text_area)
        self.default_filename = 'task_template.yaml'
        self.default_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), self.default_filename)
        self.file = None

        self.text_scrollbar.pack(side=RIGHT, fill=Y)

        # Scrollbar will adjust automatically according to the content
        self.text_scrollbar.config(command=self.text_area.yview)
        self.text_area.config(yscrollcommand=self.text_scrollbar.set)

        root.bind('<Control-Return>', self.__send_tasks)
        root.bind('<Control-a>', self.select_all)
        root.bind('<Control-s>', self.__saveFile)
        root.bind('<Control-o>', self.__openFile)
        root.bind('<Control-n>', self.__newFile)

        log.debug("Init successful")

        try:
            file = open(self.default_file, "r")

            self.text_area.insert(1.0, file.read())

            log.info(f"Opened default file {self.default_file}")

        except Exception as e:
            log.warning(f"Unable to open default file due to: {e}")
            pass

    def __openFile(self, event=None):

        log.debug("Opening file with dialog...")

        self.file = askopenfilename(defaultextension=".txt",
                                    filetypes=[("All Files", "*.*"),
                                                 ("YAML Documents", "*.yaml"),
                                                 ("Text Documents", "*.txt")])

        if self.file == "":
            # no file to open
            self.file = None
            log.debug("No file to open")

        else:
            self.text_area.delete(1.0, END)
            file = open(self.file, "r")
            self.text_area.insert(1.0, file.read())
            file.close()
            log.info(f"Opened {os.path.basename(self.file)}")

    def __newFile(self, event=None):
        self.file = None
        self.text_area.delete(1.0, END)
        log.debug("Refreshed the file in the text area")

    def __saveFile(self, event=None):
        log.debug(f"Saving file...")
        if self.file is None:
            # Save as new file
            self.file = asksaveasfilename(initialfile='Untitled.txt',
                                          defaultextension=".txt",
                                          filetypes=[("All Files", "*.*"),
                                                       ("Text Documents", "*.txt")])

            if self.file == "":
                self.file = None
            else:
                # Try to save the file
                file = open(self.file, "w")
                file.write(self.text_area.get(1.0, END))
                file.close()
                log.info(f"Saved the file as {self.file}")

        else:
            file = open(self.file, "w")
            file.write(self.text_area.get(1.0, END))
            file.close()

    def __cut(self):
        log.debug("Cut")
        self.text_area.event_generate("<<Cut>>")

    def __copy(self):
        log.debug("Copy")
        self.text_area.event_generate("<<Copy>>")

    def __paste(self):
        log.debug("Paste")
        self.text_area.event_generate("<<Paste>>")

    # Select all the text in textbox
    def select_all(self, event=None):
        log.debug("Select all")
        self.text_area.tag_add(SEL, "1.0", END)
        self.text_area.mark_set(INSERT, "1.0")
        self.text_area.see(INSERT)
        return "break"

    def __send_tasks(self, event):
        if event:
            log.debug(event)
        try:
            log.info(f"Sending tasks to Todoist!")
            tasker = TaskCreator()
            task_yaml = tasker.load_tasks_from_yaml_text(self.get_text())
            tasker.create_tasks_from_yaml(task_yaml)
            log.info(f"Done sending tasks to Todoist!")
        except Exception as e:
            log.error(f"{type(e).__name__} error sending tasks to Todoist: {e}")
        finally:
            log.debug("Break, please.")
            return "break"

    def get_text(self):
        log.debug(f"Getting text from text box...")
        return self.text_area.get(1.0, END)
