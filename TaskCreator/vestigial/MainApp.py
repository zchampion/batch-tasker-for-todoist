import tkinter as tk
from queue import Queue
from threading import Thread

from TaskCreator.vestigial.TaskerGUI import TaskerGUI
from TaskCreator.taskCreator import TaskCreator
from TaskCreator.utils import get_logger

log = get_logger(__name__)


def update_cycle(guiref, model, queue):
    while True:
        msg = queue.get()
        log.debug(f"Message from queue: {msg}")
        try:
            model_yaml = model.load_tasks_from_yaml_text(msg)
            model.create_tasks_from_yaml(model_yaml)
        except:
            log.error(f"Error creating tasks from yaml!")


def gui(root, queue):
    """
    Build a Tkinter GUI
    """
    gui = TaskerGUI(root, queue)
    gui.build()

    return gui


if __name__ == '__main__':
    root = tk.Tk()
    q = Queue()
    guiref = gui(root, q)
    model = TaskCreator()
    t = Thread(target=update_cycle, args=(guiref, model, q))
    t.daemon = True  # daemonize it, so it will die when the program stops
    t.start()
    root.mainloop()
