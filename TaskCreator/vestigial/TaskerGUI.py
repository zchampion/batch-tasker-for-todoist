"""

"""
import os
import tkinter as tk

from TaskCreator.utils import get_logger

log = get_logger(__name__)


class TaskerGUI:
    def __init__(self, root, queue):
        self.root = root
        self.queue = queue

        self.editor = tk.Text(self.root)
        self.scrollbar = tk.Scrollbar(self.root)
        self.send_tasks_button = tk.Button(text="Send Tasks", command=self.get_text)

        self.template_filename = 'basic_task.yaml'
        self.template_file = os.path.join('templates', self.template_filename)
        self.file = None

    def get_text(self, event=None):
        if event:
            log.debug(f"Event from hotkey use: {event}")
        text = self.editor.get(1.0, tk.END).strip('\n')
        self.queue.put(text)
        return "break"

    def build(self):
        self.set_metadata()

        self.editor.grid(row=0, column=0, sticky="nsew")

        self.scrollbar.grid(row=0, column=1, sticky="nsew")

        self.root.grid_rowconfigure(0, weight=1)
        self.root.grid_columnconfigure(0, weight=1)

        self.send_tasks_button.grid(row=1, column=0, sticky="nw")

        self.editor.config(yscrollcommand=self.scrollbar.set)
        self.scrollbar.config(command=self.editor.yview)

        self.set_hotkeys()

        self.open_template()

    def set_hotkeys(self):
        # Global Hotkeys
        self.root.bind_all("<Control-q>", self.quit)

        # Editor hotkeys
        self.editor.bind("<Control-Return>", self.get_text)
        self.editor.bind("<Control-a>", self.select_all)
        self.editor.bind("<Control-x>", self.cut)
        self.editor.bind("<Control-c>", self.copy)
        self.editor.bind("<Control-v>", self.paste)
        self.editor.bind("<Tab>", self.insert_spaces_for_tabs)

    def set_metadata(self, height=400, width=666):
        self.root.geometry(f"{width}x{height}")
        self.root.title("Batch Tasker for Todoist")
        self.root.minsize(height=height, width=width)

    def open_template(self):
        try:
            file = open(self.template_file, "r")

            self.editor.insert(1.0, file.read())

            log.info(f"Opened default file {self.template_file}")

        except Exception as e:
            log.warning(f"Unable to open default file due to: {e}")
            pass

    def select_all(self, event=None):
        log.debug(f"Select all{' from hotkey' if event is not None else ''}")
        self.editor.tag_add(tk.SEL, "1.0", tk.END)
        self.editor.mark_set(tk.INSERT, "1.0")
        self.editor.see(tk.INSERT)
        return "break"

    def cut(self, event=None):
        log.debug(f"Cut{' from hotkey' if event is not None else ''}")
        self.editor.event_generate("<<Cut>>")
        return "break"

    def copy(self, event=None):
        log.debug(f"Copy{' from hotkey' if event is not None else ''}")
        self.editor.event_generate("<<Copy>>")
        return "break"

    def paste(self, event=None):
        log.debug(f"Paste{' from hotkey' if event is not None else ''}")
        self.editor.event_generate("<<Paste>>")
        return "break"

    def insert_spaces_for_tabs(self, event=None, tab_size=2):
        log.debug(f"Inserting {tab_size} spaces{' from hotkey' if event is not None else ''} for tab...")
        index = self.editor.index(tk.INSERT)
        log.debug(f"Cursor is at {index}")
        self.editor.insert(index, ' '*tab_size)
        return "break"

    def quit(self, event=None):
        log.debug(f"Quit{' from hotkey' if event is not None else ''}")
        self.root.destroy()
