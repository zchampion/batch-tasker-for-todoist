import tkinter as tk
import logging
from TaskCreator.utils import get_log_level
import os


log_level = get_log_level(os.getenv('LOGLEVEL'))
if log_level is None:
    log_level = logging.INFO

logging.basicConfig(level=log_level, format='[%(asctime)s] %(levelname)s - %(message)s')
log = logging.getLogger('TaskCreator')


class ConfigWindow:
    def __init__(self, root):
        self.config_window = tk.Toplevel(root)
        self.title_label = tk.Label(self.config_window,
                                    text="Batch Creator for Todoist Configuration")
        self.fields = [
            "Todoist Key"
        ]
        self.entries = None
        self.entered_config = None
        self.submit_button = tk.Button(self.config_window, text="Save",
                                       command=self.fetch_values)
        self.clear_button = tk.Button(self.config_window, text="Clear")

    def build(self):
        self.config_window.title("Set Configuration")
        self.title_label.grid(row=0, column=0, columnspan=2)

        self.make_entries()

        self.submit_button.grid(column=1, sticky=tk.E)
        self.clear_button.grid(column=1, sticky=tk.E)

    def fetch_values(self):
        self.entered_config = {entry[0]: entry[1].get() for entry in self.entries}
        log.debug(f'Config from window: {self.entered_config}')
        return self.entered_config

    def make_entries(self):
        self.entries = []

        for field in self.fields:
            row = tk.Frame(self.config_window)
            label = tk.Label(row, width=15, text=field, anchor='w')
            entry = tk.Entry(row)
            row.grid()
            label.pack(side=tk.LEFT)
            entry.pack(side=tk.LEFT)
            self.entries.append((field, entry))
