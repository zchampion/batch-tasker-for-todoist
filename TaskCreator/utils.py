import logging
import os


def get_log_level():
    level = "INFO"
    log_level_var_names = [
        'LOGLEVEL',
        'LOG_LEVEL'
    ]

    for name in log_level_var_names:
        if name in os.environ:
            level = os.getenv(name)
            break

    if level.upper() == 'DEBUG':
        return logging.DEBUG
    elif level.upper() == 'INFO':
        return logging.INFO
    elif level.upper() == 'WARNING':
        return logging.WARNING
    elif level.upper() == 'ERROR':
        return logging.ERROR
    elif level.upper() == 'CRITICAL':
        return logging.CRITICAL
    elif int(level) in [0, 10, 20, 30, 40, 50]:
        return int(level)
    else:
        raise ValueError(f"Invalid log level found: {level}")


def get_logger(name):
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s | %(name)s | %(levelname)8s: %(message)s')
    log = logging.getLogger(name)
    if 'LOGLEVEL' in os.environ:
        log.setLevel(logging._nameToLevel[os.getenv('LOGLEVEL').upper()])
    return log
