import os
import sys
from datetime import datetime
from functools import partial

from PyQt5.QtCore import Qt, QObject, pyqtSignal, QThread
from PyQt5.QtGui import QKeySequence, QFont
from PyQt5.QtWidgets import QTextEdit, QApplication, QMainWindow, QMenu, QToolBar, QAction, QFileDialog

from TaskCreator.taskCreator import TaskCreator
from TaskCreator.utils import get_logger

log = get_logger(__name__)


class Window(QMainWindow):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setWindowTitle(f"Batch Tasker for Todoist")
        self.resize(1000, 600)
        self.centralWidget = QTextEdit()
        self.setCentralWidget(self.centralWidget)
        self.configureCentralWidget()
        self.resetCentralWidget()

        try:
            self.taskCreator = TaskCreator()

        except Exception as e:
            msg = f"Error setting up the task creator! {type(e).__name__}: {e}"
            log.error(msg)
            self.centralWidget.setPlainText(msg)
            self.centralWidget.setReadOnly(True)

        self._createActions()
        self._createMenuBar()
        self._createToolBars()
        self._createContextMenu()
        self._connectActions()
        self._createStatusBar()

    def _createMenuBar(self):
        log.debug(f"Creating menus...")
        menuBar = self.menuBar()
        log.debug(f"File menu...")
        fileMenu = QMenu("&File", self)
        menuBar.addMenu(fileMenu)
        fileMenu.addAction(self.newAction)
        fileMenu.addAction(self.openAction)
        fileMenu.addAction(self.saveAction)
        fileMenu.addAction(self.appendNewAction)
        fileMenu.addAction(self.sendTasksAction)
        fileMenu.addSeparator()
        fileMenu.addAction(self.exitAction)
        # Creating menus using a title
        log.debug(f"Edit menu...")
        editMenu = menuBar.addMenu("&Edit")
        editMenu.addAction(self.cutAction)
        editMenu.addAction(self.copyAction)
        editMenu.addAction(self.pasteAction)
        editMenu.addSeparator()
        # Find and Replace submenu in the Edit menu
        findMenu = editMenu.addMenu("Find and Replace")
        findMenu.addAction("Find...")
        findMenu.addAction("Replace...")
        log.debug(f"Insert menu...")
        self.insertMenu = menuBar.addMenu("&Insert")
        self.projectMenu = self.insertMenu.addMenu("Projects")
        self.labelMenu = self.insertMenu.addMenu("Labels")
        log.debug(f"Help menu...")
        helpMenu = menuBar.addMenu("&Help")
        helpMenu.addAction(self.helpContentAction)
        helpMenu.addAction(self.aboutAction)

    def _createToolBars(self):
        log.debug(f"Creating toolbars...")
        # Using a title
        fileToolBar = self.addToolBar("File")
        fileToolBar.addAction(self.newAction)
        fileToolBar.addAction(self.openAction)
        fileToolBar.addAction(self.saveAction)
        fileToolBar.addAction(self.appendNewAction)
        fileToolBar.addAction(self.sendTasksAction)
        # Using a QToolBar object
        editToolBar = QToolBar("Edit", self)
        self.addToolBar(editToolBar)
        editToolBar.addAction(self.cutAction)
        editToolBar.addAction(self.copyAction)
        editToolBar.addAction(self.pasteAction)
        # Labels & Project Names
        self.projectsToolBar = QToolBar("Projects", self)
        self.addToolBar(Qt.RightToolBarArea, self.projectsToolBar)
        self.labelToolBar = QToolBar("Labels", self)
        self.addToolBar(Qt.LeftToolBarArea, self.labelToolBar)
        self.populateValidProjects()
        self.populateValidLabels()

    def _createActions(self):
        log.debug(f"Creating actions for menus & toolbars...")
        # File Actions
        # Creating action using one type of constructor
        self.newAction = QAction(self)
        self.newAction.setText("&New")
        newTip = "Create a new file"
        self.newAction.setStatusTip(newTip)
        self.newAction.setToolTip(newTip)
        # Creating actions using the second type of constructor
        self.openAction = QAction("&Open...", self)
        self.saveAction = QAction("&Save", self)
        self.appendNewAction = QAction("&Append New")
        appendNewTip = "Append a new document to the current text"
        self.appendNewAction.setStatusTip(appendNewTip)
        self.appendNewAction.setToolTip(appendNewTip)
        self.exitAction = QAction("&Exit", self)
        # Shortcuts
        self.newAction.setShortcut("Ctrl+N")
        self.openAction.setShortcut("Ctrl+O")
        self.saveAction.setShortcut("Ctrl+S")
        # Edit Actions
        self.cutAction = QAction("C&ut", self)
        self.copyAction = QAction("&Copy", self)
        self.pasteAction = QAction("&Paste", self)
        self.helpContentAction = QAction("&Help Content", self)
        self.aboutAction = QAction("&About", self)
        # Shortcuts
        self.cutAction.setShortcut(QKeySequence.Cut)
        self.copyAction.setShortcut(QKeySequence.Copy)
        self.pasteAction.setShortcut(QKeySequence.Paste)
        # Other
        self.sendTasksAction = QAction("Send Tasks to &Todoist", self)
        self.sendTasksAction.setShortcut("Ctrl+Return")
        self.sendTasksAction.setStatusTip(f"Send the tasks to Todoist")

    def _createContextMenu(self):
        log.debug(f"Creating central widget context menu...")
        # Setting contextMenuPolicy
        self.centralWidget.setContextMenuPolicy(Qt.ActionsContextMenu)
        # Populating the widget with actions
        self.centralWidget.addAction(self.newAction)
        self.centralWidget.addAction(self.openAction)
        self.centralWidget.addAction(self.saveAction)
        self.centralWidget.addAction(self.appendNewAction)
        self.centralWidget.addAction(self.copyAction)
        self.centralWidget.addAction(self.pasteAction)
        self.centralWidget.addAction(self.cutAction)
        self.centralWidget.addAction(self.sendTasksAction)

    def _connectActions(self):
        log.debug(f"Connecting actions to slots...")
        # Connect File actions
        self.newAction.triggered.connect(self.newFile)
        self.openAction.triggered.connect(self.openFile)
        self.saveAction.triggered.connect(self.saveFile)
        self.appendNewAction.triggered.connect(self.appendNewDoc)
        self.exitAction.triggered.connect(self.close)
        self.exitAction.setShortcut("Ctrl+Q")
        # Connect Edit actions
        self.copyAction.triggered.connect(self.copyContent)
        self.pasteAction.triggered.connect(self.pasteContent)
        self.cutAction.triggered.connect(self.cutContent)
        # Connect Help actions
        self.helpContentAction.triggered.connect(self.helpContent)
        self.aboutAction.triggered.connect(self.about)
        # Other
        self.sendTasksAction.triggered.connect(self.sendTasks)

    def _createStatusBar(self):
        self.statusBar = self.statusBar()
        # Adding a temporary message
        self.statusBar.showMessage("Ready", 3000)

    def configureCentralWidget(self):
        monospace = QFont('Courier', 12, QFont.Monospace)
        self.centralWidget.setFont(monospace)

    def resetCentralWidget(self):
        newText = open(os.path.join(os.path.abspath(os.curdir), 'TaskCreator', 'templates', 'basic_task.yaml')).read()
        log.debug(f"New text is:\n{newText}")
        self.centralWidget.setPlainText(newText)

    def newFile(self):
        # Logic for creating a new file goes here...
        msg = "File > New clicked!"
        log.info(msg)
        self.resetCentralWidget()

    def openFile(self):
        # Logic for opening an existing file goes here...
        msg = "File > Open... clicked!"
        log.warning(msg)
        name = QFileDialog.getOpenFileName(self, 'Save File')[0]
        if name:
            log.debug(f"name is '{name}'")
            with open(name) as opened:
                self.centralWidget.setText(opened.read())
            log.info(f"Opened {name}")

    def saveFile(self):
        # Logic for saving a file goes here...
        msg = "File > Save clicked!"
        log.debug(msg)
        name = QFileDialog.getSaveFileName(self, 'Save File')[0]
        if name:
            log.debug(f"Save name: {name}")
            with open(name, 'w') as saver:
                saver.write(self.centralWidget.toPlainText())
            log.info(f"Saved file as {name}")

    def appendNewDoc(self):
        msg = "File > Append New clicked!"
        log.info(msg)
        newText = open(os.path.join(os.path.abspath(os.curdir), 'TaskCreator', 'templates', 'basic_task.yaml')).read()
        self.centralWidget.append(newText)

    def cutContent(self):
        # Logic for cutting content goes here...
        msg = "Edit > Cut clicked!"
        log.info(msg)
        self.centralWidget.cut()

    def copyContent(self):
        # Logic for copying content goes here...
        msg = "Edit > Copy clicked!"
        log.info(msg)
        self.centralWidget.copy()

    def pasteContent(self):
        # Logic for pasting content goes here...
        msg = "Edit > Paste clicked!"
        log.info(msg)
        self.centralWidget.paste()

    def helpContent(self):
        # Logic for launching help goes here...
        msg = "Help > Help Content... clicked!"
        log.info(msg)

    def about(self):
        # Logic for showing an about dialog content goes here...
        msg = "Help > About... clicked!"
        log.info(msg)

    def populateValidProjects(self):
        try:
            self.projectMenu.clear()
        except:
            pass
        actions = []
        for project in sorted(self.taskCreator.valid_projects):
            if len(project) > 10:
                metalabel = project[:10] + '...'
            else:
                metalabel = project
            log.debug(f"Metalabel for project {project} = {metalabel}")
            action = QAction(metalabel, self)
            action.triggered.connect(partial(self.insertText, project))
            action.setToolTip(str(project))
            actions.append(action)
        self.projectsToolBar.addActions(actions)
        self.projectMenu.addActions(actions)

    def populateValidLabels(self):
        try:
            self.labelMenu.clear()
        except:
            pass
        actions = []
        for label in sorted(self.taskCreator.valid_labels):
            if len(label) > 10:
                metalabel = label[:10] + '...'
            else:
                metalabel = label
            log.debug(f"Metalabel for label {label} = {metalabel}")
            action = QAction(metalabel, self)
            action.triggered.connect(partial(self.insertText, label))
            action.setToolTip(label)
            actions.append(action)
        self.labelToolBar.addActions(actions)
        self.labelMenu.addActions(actions)

    def insertText(self, text):
        log.debug(f"Inserting {text}")
        self.centralWidget.insertPlainText(text)

    def updateStatus(self, stringMessage):
        log.debug(f"Updating status bar with '{stringMessage}'")
        self.statusBar.showMessage(stringMessage)

    def sendTasks(self):
        self.sendTasksThread = QThread()
        self.sendTasksWorker = TaskSenderWorker()
        self.sendTasksWorker.taskProcessor = self.taskCreator
        self.sendTasksWorker.text = self.centralWidget.toPlainText()
        self.sendTasksWorker.moveToThread(self.sendTasksThread)

        self.sendTasksThread.started.connect(self.sendTasksWorker.run)
        self.sendTasksWorker.finished.connect(self.sendTasksThread.quit)
        self.sendTasksWorker.finished.connect(self.sendTasksWorker.deleteLater)
        self.sendTasksThread.finished.connect(self.sendTasksThread.deleteLater)
        self.sendTasksWorker.barStatus.connect(self.updateStatus)
        self.sendTasksWorker.nonbarStatus.connect(self.updateStatus)

        self.sendTasksThread.start()


class TaskSenderWorker(QObject):
    taskProcessor = None
    text = None
    barStatus = pyqtSignal(str)
    nonbarStatus = pyqtSignal(str)
    finished = pyqtSignal()

    def run(self):
        log.debug(f"Sending tasks to Todoist...\n{self.text}")
        yamlTasks = self.taskProcessor.load_tasks_from_yaml_text(self.text)
        try:
            for i, document in enumerate(yamlTasks, start=1):
                log.info(f"Creating tasks from document {i}...")
                self.taskProcessor.create_tasks_from_yaml(document, status_signal=self.barStatus)
        except Exception as e:
            log.critical(f"Failed to create tasks from yaml due to {type(e).__name__}: {e}")
            log.debug(f"Dumping task text...")
            try:
                with open(f"error_dump_{datetime.now().isoformat()}.txt", 'w') as error_pen:
                    error_pen.write(self.text)
            except Exception as e2:
                log.critical(f"Failed to dump task text due to {type(e2).__name__}: {e2}")

        log.debug(f"Emitting task sender worker finished")
        self.finished.emit()


if __name__ == "__main__":
    log.info(f"Starting!")
    qpp = QApplication(sys.argv)
    win = Window()
    win.show()
    code = qpp.exec()
    log.info(f"Exiting.")
    log.debug(f"Exit code: {code}")
    sys.exit(code)