#!/usr/bin/env python3

import logging
import os
import re
import sys

logging.basicConfig(level=logging.INFO, format='%(asctime)s | %(name)s | %(levelname)8s: %(message)s')
log = logging.getLogger(__name__)
if 'LOGLEVEL' in os.environ:
    log.setLevel(logging._nameToLevel[os.getenv('LOGLEVEL').upper()])


MAP_FILE = '/home/queen/Desktop/map.txt'
FILE2WRITE = '/home/queen/Desktop/tasks.yaml'


def read_map(map_file):
    log.info(f"Reading the file...")

    try:
        with open(map_file) as reader:
            text = reader.read()
            log.debug(f"Text from file: {text}")

        return text

    except FileNotFoundError:
        msg = f"File {map_file} not found."
        log.error(msg)
        sys.exit(1)

    except Exception as e:
        msg = f"Error reading the map file due to {type(e).__name__}: {e}"
        log.error(msg)
        raise e


def write_map_tasks(text):
    try:
        log.debug(f"Tasks: {text}")
        log.info(f"Writing the new file...")
        with open(FILE2WRITE, 'w') as pen:
            pen.write(text)

    except Exception as e:
        msg = f"Error writing the map tasks due to {type(e).__name__}: {e}"
        log.error(msg)
        raise e


def convert_text(tasks):
    log.debug(f"Subbing the numbering with dashes...")
    tasks = re.sub(r'(?:\d+\.)+\d+', '-', tasks)
    tasks = line_by_line(tasks)
    log.debug(f"Subbing the tabs for spaces...")
    tasks = re.sub(r'\t', '  ', tasks)
    return tasks


def line_by_line(text):
    log.debug(f"Altering line by line (adding colons to tasks with subtasks)...")

    new_text = ''
    lines = text.split('\n')
    log.debug(f"Length of lines: {len(lines)}")

    for line_id, line in enumerate(lines):
        tab_count = lines[line_id].count('\t')
        log.debug(f"Tab count on line {line_id}: {tab_count}")
        if line_id < len(lines) - 2:
            next_tab_count = lines[line_id + 1].count('\t')
            log.debug(f"Next tab count on line {line_id}: {next_tab_count}")
            if next_tab_count > tab_count:
                log.debug(f"Adding colon to: {line}")
                new_text += line + ':\n'
            else:
                log.debug(f"Just appending: {line}")
                new_text += line + '\n'
        else:
            log.debug(f"Just appending as last line:  {line}")
            new_text += line + '\n'

    return new_text


if __name__ == "__main__":
    map_text = read_map(MAP_FILE)
    text_tasks = convert_text(map_text)
    write_map_tasks(text_tasks)
